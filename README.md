## Как развернуть инфраструктуру для работы приложения
1. Клонировать репозиторий infra и развернуть необходимую инфраструктуру
   https://gitlab.com/skillbox_diploma/infra.git

2. Клонировать репозиторий service (репозиторий имеет 2 ветки: main и uat) (должен распологаться в том же каталоге что и репозиторий infra)
   https://gitlab.com/skillbox_diploma/service.git

3. Экспортировать данные для доступа к ресурсам AWS (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY). 
   ```shell
   export AWS_ACCESS_KEY_ID=[указать значение]
   export AWS_SECRET_ACCESS_KEY=[указать значение]
   ``` 
4. Перейти в каталог terraform репозитория infra, прописать имя своего ssh ключа в файле main.tf и запустить план для разворачивания инфраструктуры командой 
   ```shell
   terraform apply --auto-approve
   ```
5. Адреса развернутых серверов, необходимые для дальнейшей настройки инфраструктуры будут автоматически внесены в файл hosts для ansible (скрипт get_ip.sh выполнится в процессе работы terraform). Также этот скрипт скопирует файл hosts в каталог репозитория service, подкаталог ansible, для разворачивания шаблона приложения.

6. Перейти в каталог ansible репозитория infra и запустить настройку инфраструктуры (установка всего, необходимого для работы приложения, ПО) 
   - установка Docker
   ```shell
   ansible-playbook install_docker.yml
   ```
   - регистрация необходимых раннеров на Gitlab.com (прописать токены для регистрации раннера на сервере gitlab.com в переменную reg_token плэйбука register_gitlab_runner.yml)
   ```shell
   ansible-playbook register_gitlab_runner.yml
   ```
   - создание пары ssh ключей для серверов приложений и сервера с gitlab-runner (для возможности деплоя). Имя сервера gitlab-runner (gtlb_runner_1) жестко внесено в файл create_ssh_keys.yml 
   ```shell
   ansible-playbook create_ssh_keys.yml
   ```







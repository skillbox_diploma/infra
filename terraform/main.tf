
provider "aws" {
    region = "eu-central-1"
}

data "aws_availability_zones" "vmsap_available_zones" {}

resource "aws_default_subnet" "vmsap_available_zone_1" {
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[0]
}

resource "aws_default_subnet" "vmsap_available_zone_2" {
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[1]
}



data "aws_ami" "vmsap_ubuntu" {
  owners = ["099720109477"]
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_security_group" "vmsap_sg_rules" {
    name = "vmsap_sg_rules"
    dynamic "ingress" {
    for_each = ["80", "22", "3000", "9090", "9093", "9091", "9100", "8080", "9252"]
    content {
        from_port = ingress.value
        to_port = ingress.value
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
      Name = "Access to vmsap"
  }
}

resource "aws_launch_configuration" "vmsap_lc" {
  name_prefix = "vmsap-"
  image_id = data.aws_ami.vmsap_ubuntu.id
  instance_type = "t2.micro"
  security_groups = [aws_security_group.vmsap_sg_rules.id]
  user_data = file("./scripts/webapp_user_data.sh")
  key_name = "ubnt20-home"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "vmsap_asg" {
  name = "VMSAP-ASG-${aws_launch_configuration.vmsap_lc.name}"
  launch_configuration = aws_launch_configuration.vmsap_lc.name
  min_size = 1
  max_size = 1
  min_elb_capacity = 1
  health_check_type = "ELB"
  depends_on = [aws_instance.vmsap_gitlab_runner, aws_instance.vmsap_monitoring] // ждем создания сервера для gitlab-runner и, 
                                                                                 // чтобы собрать потом все ip-адреса     
  vpc_zone_identifier = [aws_default_subnet.vmsap_available_zone_1.id, aws_default_subnet.vmsap_available_zone_2.id]

  // script to add ip of created servers to ansible hosts file
  provisioner "local-exec" {
    command = "./scripts/get_ip.sh"
  }

  dynamic "tag" {
    for_each = {
      Name = "Webapp Server in VMSAP-ASG"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Application Load Balancer


resource "aws_lb" "vmsap_alb" {
  name               = "VMSAP-ALB"
  subnets = [aws_default_subnet.vmsap_available_zone_1.id, aws_default_subnet.vmsap_available_zone_2.id]
  internal           = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.vmsap_sg_rules.id]

  tags = {
    Name        = "WebApp VmsAp"
    Environment = "Dev"
  }
}

# Attachments to ALB for target groups

resource "aws_autoscaling_attachment" "vmsap_webapp_attachment_lb" {
  autoscaling_group_name = aws_autoscaling_group.vmsap_asg.id
  alb_target_group_arn = aws_lb_target_group.webapp_vmsap_tg.arn
}

resource "aws_lb_target_group_attachment" "vmsap_monitoring_attachment_lb" {
  #autoscaling_group_name = aws_autoscaling_group.vmsap_asg.id
  target_group_arn = aws_lb_target_group.monitoring_vmsap_tg.arn
  target_id = aws_instance.vmsap_monitoring.id
}

resource "aws_lb_target_group_attachment" "vmsap_grafana_attachment_lb" {
  #autoscaling_group_name = aws_autoscaling_group.vmsap_asg.id
  target_group_arn = aws_lb_target_group.grafana_vmsap_tg.arn
  target_id = aws_instance.vmsap_monitoring.id
}

# ALB listeners

resource "aws_lb_listener" "vmsap_webapp_listener" {
  load_balancer_arn = "${aws_lb.vmsap_alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.webapp_vmsap_tg.arn}"
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "monitoring_listener_rule" {
  listener_arn = aws_lb_listener.vmsap_webapp_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.monitoring_vmsap_tg.arn
  }

  condition {
    host_header {
      values = ["monitoring.vmsap.tk"]
    }
  }
}

resource "aws_lb_listener_rule" "grafana_listener_rule" {
  listener_arn = aws_lb_listener.vmsap_webapp_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.grafana_vmsap_tg.arn
  }

  condition {
    host_header {
      values = ["grafana.vmsap.tk"]
    }
  }
}


# ALB target groups

resource "aws_lb_target_group" "webapp_vmsap_tg" {
  name        = "vmsap-webapp-targetgroup"
  port        = 80
  protocol    = "HTTP"
  vpc_id = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"

  health_check {
    interval            = 70
    path                = "/"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    protocol            = "HTTP"
    matcher             = "200,202"
  }
}

resource "aws_lb_target_group" "monitoring_vmsap_tg" {
  name        = "prometheus-webapp-targetgroup"
  port        = 9090
  protocol    = "HTTP"
  vpc_id = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"

resource "aws_lb_target_group" "grafana_vmsap_tg" {
  name        = "grafana-webapp-targetgroup"
  port        = 3000
  protocol    = "HTTP"
  vpc_id = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"


// Gitlab-runner service

resource "aws_instance" "vmsap_gitlab_runner" {
  ami                    = data.aws_ami.vmsap_ubuntu.id
  instance_type          = "t2.micro"
  key_name               = "ubnt20-home"
  vpc_security_group_ids = [aws_security_group.vmsap_sg_rules.id]
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[1]

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "echo 1",
      "sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner unregister --name vmsap-docker-runner",
      "sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner unregister --name vmsap-ansible-runner",
      "echo 2",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("/home/aeri/.ssh/id_rsa")
      host        = self.public_ip
    }
  }

  tags = {
    Name = "Gitlab-Runner Server"
  }

  lifecycle {
    create_before_destroy = true
  }
}

// Monitoring service

resource "aws_instance" "vmsap_monitoring" {
  ami                    = data.aws_ami.vmsap_ubuntu.id
  instance_type          = "t3.micro"
  key_name               = "ubnt20-home"
  vpc_security_group_ids = [aws_security_group.vmsap_sg_rules.id]
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[0]
  
  tags = {
    Name = "Monitoring Server"
  }
  
  lifecycle {
    create_before_destroy = true
  }
}

// Route53 service

data "aws_route53_zone" "vmsap_hosted_zone" {
  name         = "vmsap.tk."
  private_zone = false
}

resource "aws_route53_record" "test-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "test.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  type    = "A"

  alias {
    name = aws_lb.vmsap_alb.dns_name
    zone_id = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "monitoring-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "monitoring.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  type    = "A"

  alias {
    name = aws_lb.vmsap_alb.dns_name
    zone_id = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "grafana-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "grafana.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  type    = "A"

  alias {
    name = aws_lb.vmsap_alb.dns_name
    zone_id = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}


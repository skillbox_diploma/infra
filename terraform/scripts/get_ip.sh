#!/bin/bash

#  собираем адреса серверов приложений

webapp_arr=($(aws ec2 describe-instances --region eu-central-1  --filters Name=tag:Name,Values="Webapp Server in VMSAP-ASG" --query 'Reservations[*].Instances[].PublicIpAddress' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' -))

#rm ../ansible/dockprom/prometheus/webapp_nodeexporter_targets.yml # удаляем старый и формируем targets файл для prometheus скрэппинга для webapp
#rm ../ansible/dockprom/prometheus/webapp_cadvisor_targets.yml

target_nodeexp="- targets: [" 
target_cadvisor="- targets: ["
target_webapp="- targets: [" 
ip_target_nodeexp=""
ip_target_cadvisor=""
ip_target_webapp=""

echo "[webapp_servers]" > ../ansible/hosts
i=1
for item in ${webapp_arr[*]}
do
    echo "webapp_"$i"  ansible_host="$item"  ansible_user=ubuntu" >> ../ansible/hosts
    
    ip_target_nodeexp=$ip_target_nodeexp"'"$item":9100',"
    ip_target_cadvisor=$ip_target_cadvisor"'"$item":8080',"
    ip_target_webapp=$ip_target_webapp"'"$item":80',"
    
    let i=i+1
done

target_nodeexp=$target_nodeexp$ip_target_nodeexp"]"
target_cadvisor=$target_cadvisor$ip_target_cadvisor"]"
target_webapp=$target_webapp$ip_target_webapp"]"

echo $target_nodeexp > ../ansible/dockprom/prometheus/webapp_nodeexporter_targets.yml
echo $target_cadvisor > ../ansible/dockprom/prometheus/webapp_cadvisor_targets.yml
echo $target_webapp > ../ansible/dockprom/prometheus/webapp_target.yml
echo $target_webapp > ../ansible/dockprom/prometheus/webapp_blackbox.yml

# собираем адреса серверов gitlab-runner

gtlb_arr=($(aws ec2 describe-instances --region eu-central-1  --filters Name=tag:Name,Values="Gitlab-Runner Server" --query 'Reservations[*].Instances[].PublicIpAddress' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' -))

rm ../ansible/dockprom/prometheus/gtlb_runner_nodeexporter_targets.yml # удаляем старый и формируем targets файл для prometheus скрэппинга для webapp
rm ../ansible/dockprom/prometheus/gtlb_runner_cadvisor_targets.yml

target_nodeexp="- targets: ["
target_cadvisor="- targets: ["
target_gtlb_service="- targets: ["
ip_target_nodeexp=""
ip_target_cadvisor=""
ip_target_gtlb_service=""

echo "" >> ../ansible/hosts
echo "[gitlab_runner_servers]" >> ../ansible/hosts
i=1
for item in ${gtlb_arr[*]}
do
    echo "gtlb_runner_"$i"  ansible_host="$item"  ansible_user=ubuntu" >> ../ansible/hosts
    
    ip_target_nodeexp=$ip_target_nodeexp"'"$item":9100',"
    ip_target_cadvisor=$ip_target_cadvisor"'"$item":8080',"
    ip_target_gtlb_service=$ip_target_gtlb_service"'"$item":9252',"
    
    let i=i+1
done

target_nodeexp=$target_nodeexp$ip_target_nodeexp"]"
target_cadvisor=$target_cadvisor$ip_target_cadvisor"]"
target_gtlb_service=$target_gtlb_service$ip_target_gtlb_service"]"

echo $target_nodeexp > ../ansible/dockprom/prometheus/gtlb_runner_nodeexporter_targets.yml
echo $target_cadvisor > ../ansible/dockprom/prometheus/gtlb_runner_cadvisor_targets.yml
echo $target_gtlb_service > ../ansible/dockprom/prometheus/gtlb_runner_runners_targets.yml


# собираем адреса серверов мониторинга

mon_arr=($(aws ec2 describe-instances --region eu-central-1  --filters Name=tag:Name,Values="Monitoring Server" --query 'Reservations[*].Instances[].PublicIpAddress' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' -))

echo "" >> ../ansible/hosts
echo "[monitoring_servers]" >> ../ansible/hosts
i=1
for item in ${mon_arr[*]}
do
    echo "monitoring_"$i"  ansible_host="$item"  ansible_user=ubuntu" >> ../ansible/hosts
    let i=i+1
done

cp ../ansible/hosts ../../service/ansible/hosts

